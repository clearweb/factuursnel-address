<?php namespace Factuursnel\Address;

class Address extends \Eloquent
{
	protected $table = 'address';

    protected $fillable = [
        'street',
        'number',
        'extension',
        'postal_code',
        'city',
        'country',
    ];
}